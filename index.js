import React from 'react'
import ReactDOM from 'react-dom'
import { groupByCategory, groupByCategories, pivot } from './src'
import { collapse } from './src/utils'

const data = [
	{color: "blue", gender: "male", height: 'small', shape: "circle", value: 1, 'value2': 3},
	{color: "red", gender: "female", height: 'medium', shape: "triangle", value: 2, 'value2': 2},
	{color: "red", gender: "male", height: 'large', shape: "circle", value: 3, 'value2': 1},
	{color: "red", gender: "male", height: 'medium', shape: "circle", value: 3, 'value2': 1},
	{color: "blue", gender: "male", height: 'large', shape: "triangle", value: 4, 'value2': 2},
	{color: "blue", gender: "female", height: 'small', shape: "triangle", value: 4, 'value2': 2},
	{color: "green", gender: "female", height: 'large', shape: "triangle", value: 4, 'value2': 2}
]

const { dataRows } = pivot({
	data, 
	cols: [],
	rows: ['height', 'shape'],
	visibleColumns: ['shape', 'color'],
	// visibleColumns: [],
	// aggs: ['value'],
	aggs: ['value', 'value2'],
	aggregator: ['sum', 'sum']
})

console.log(dataRows)
console.log(collapse(4, dataRows))

class App extends React.Component {
	render () {
		return (
			<table className='u-full-width'>
				<thead>
				</thead>
				<tbody>
					{dataRows.map(({ value, type, depth }) => {
						return (
							<tr>
								{value.map((val, idx) => {
									return (
										<td>{type === 'rowHeader' && idx === 0 ? <strong style={{marginLeft: depth * 10}}>{val}</strong> : <span style={{marginLeft: idx === 0 ?  depth * 10 : 0}}>{val}</span>}</td>
									)
								})}
							</tr>
						)
					})}
				</tbody>
			</table>
		)
	}
}


ReactDOM.render(<App />, document.getElementById('root'))