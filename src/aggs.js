/**
 * @description Reduces an array of values to a result
 * @param {!Array} arr The array to reduce
 * @param {?requestCallback|string} accCat Callback, category to reduce, or null
 * @param {string} accType Reduce type (count, average, min, max, etc) or initial value
 * @param {*} accValue Initial value - string, number, array, or object
 * @returns {*} Reduced value
 * @todo Move accumulator to its own file since it will continue to grow
*/
export function accumulator(arr, accCat, accType = 'sum', accValue) {
  if (typeof accCat === 'undefined') accType = 'count';
  else if (typeof accCat === 'function') accValue = accType || 0;

  return arr.reduce((acc, curr, index, array) => {
    if (typeof accCat === 'function') return accCat(acc, curr, index, array);

    switch (accType) {
      case ('average'): {
        acc += Number(curr[accCat]) / arr.length;

        return acc;
      }

      case ('count'): {
        acc += 1;

        return acc;
      }

      case ('min'): {
        if (index === 0) acc = Number(curr[accCat]);
        else if (curr[accCat] < acc) acc = Number(curr[accCat]);

        return acc;
      }

      case ('max'): {
        if (index === 0) acc = Number(curr[accCat]);
        else if (curr[accCat] > acc) acc = Number(curr[accCat]);

        return acc;
      }

      case ('sum'): {
        acc += Number(curr[accCat]);

        return acc;
      }

      default: {
        acc += 1;

        return acc;
      }
    }
  }, accValue || 0);
}