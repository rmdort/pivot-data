import { accumulator } from './aggs'
import { collapse } from './utils'

/**
 * Group by category
 * @param  {[type]} data    [description]
 * @param  {[type]} groupBy [description]
 * @return {[type]}         [description]
 */
export function groupByCategory(data, groupBy) {
  return data.reduce((acc, curr) =>{
    const category = curr[groupBy];

    if (!acc[category]) acc[category] = [];
    acc[category].push(curr);
    return acc;
  }, {});
}

/**
 * Group by multiple categories
 * @param  {[type]} data       [description]
 * @param  {Array}  groups     [description]
 * @param  {Object} acc        [description]
 * @param  {[type]} accCatOrCB [description]
 * @return {[type]}            [description]
 */
export function groupByCategories(data, groups = [], acc = {}) {
  /**
   * base case - if data is empty
   * or if there are no more groups to group by
   * return result
  */
  if (data.length === 0 || groups.length === 0) return data

  const groupedData = groupByCategory(data, groups[0]);  
  const groupedDataKeys = Object.keys(groupedData);  
  const children = Object.values(groupedData);

  for (let i = 0; i < children.length; i++) {
    acc[groupedDataKeys[i]] = groupByCategories(
        children[i], groups.slice(1), acc[groupedDataKeys[i]]);
  }

  return acc;
}

export function fillArray (len) {
  return Array.from({ length: len}).map((i) => '')
}

/**
 * @description Builds the column headers and a map to each header
 * @param {!Array<Object>} data Array of objects
 * @param {Array} cols Columns to pivot on
 * @param {string} firstColumn A string to place in the first column header
 * @returns {Object} columnHeaders (array of arrays) and mapToHeader (object)
*/
export function createColumnHeaders(data, cols = [], firstColumn = '') {
  if (cols.length === 0) {
    return {
      columnHeaders: [firstColumn],
      mapToHeader: null,
    };
  }

  const mapToHeader = groupByCategories(data, cols);
  const columnHeaders = [];
  let mapPos = 1;

  (function columnHeaderRecursion(data, pos = 0, headerMap) {
    /** base case - at actual data as opposed to another grouping */
    if (typeof data !== 'object' || Array.isArray(data)) return 1;

    const currKeys = Object.keys(data);
    let sumLength = 0;

    for (let i = 0; i < currKeys.length; i++) {
      const currLength = columnHeaderRecursion(
          data[currKeys[i]], pos + 1, headerMap[currKeys[i]]);

      if (Array.isArray(data[currKeys[i]])) {
        headerMap[currKeys[i]] = mapPos;
        mapPos += 1;
      }

      columnHeaders[pos] = typeof columnHeaders[pos] === 'undefined' ?
        [firstColumn].concat(currKeys[i], fillArray(currLength - 1)) :
        columnHeaders[pos].concat(currKeys[i], fillArray(currLength - 1));

      sumLength += currLength;
    }

    return sumLength;

  }(mapToHeader, 0, mapToHeader));

  return {
    columnHeaders,
    mapToHeader,
  };
}

/**
 * Pivot a dataset
 * @param  {[type]} options.data           [description]
 * @param  {Array}  options.cols           [description]
 * @param  {Array}  options.rows           [description]
 * @param  {Array}  options.visibleColumns [description]
 * @param  {Array}  options.aggs           [description]
 * @return {[type]}                        [description]
 */
export function pivot ({
	data,
	cols = [],
	rows = [],
	visibleColumns = [],
	aggs=[],
	aggregator = []
}) {

	/**
	 * Columns headers
	 */
	let dataRows = []
	let prevKey = null

	if (cols.length) {
		visibleColumns = []
		rows = rows.filter((item) => item !== 'data')
	} else {
		rows = [...rows, 'data']
	}

	const columnData = createColumnHeaders(data, cols);
  const columnHeaders = Array.isArray(columnData.columnHeaders[0]) ?
    columnData.columnHeaders :
    [columnData.columnHeaders.concat(visibleColumns.filter((_, i) => i > 0), aggs.map((item) => item))];

  const mapToHeader = columnData.mapToHeader
  const headerLength = columnHeaders[0].length
  const formattedColumnHeaders = columnHeaders.map((value, depth) => {
    return {
      value,
      depth,
      type: 'colHeader',
      row: depth,
    };
  });


	function rowRecurse(rowGroups, depth, rowHeaders = []) {
    for (const key in rowGroups) {
      if (Array.isArray(rowGroups[key])) {
        const recursedData = groupByCategories(rowGroups[key], cols);

        prevKey = null;

        (function recurseThroughMap(dataPos, map) {
          if (Array.isArray(dataPos)) {
            if (key === prevKey) {
              /* Calculate */
              const datum = dataRows[dataRows.length - 1].value;

              if (cols.length) {
              	const datum = dataRows[dataRows.length - 1].value;
	              datum[map] = accumulator(dataPos, aggs[0]);
	              dataRows[dataRows.length - 1].value = datum
              }
            } else {
              prevKey = key;
              if (key !== 'undefined') {

              	if (!cols.length) {
	              	dataRows.push({
	              		value: [
	              			key,
	              			...fillArray(visibleColumns.length - 1),
	              			...aggs.map((field, idx) => accumulator(dataPos, field, aggregator[idx]))
	              		],
	              		type: 'data',
	              		depth,
	              		row: dataRows.length + formattedColumnHeaders.length,
	              	})
	              } else {

	              	const datum = [key].concat(
			                Array(map - 1).fill(''),
			                accumulator(dataPos, aggs[0]),
			                Array(headerLength - (map + 1)).fill('')
			              )
	              	dataRows.push({
	              		value: datum,
	              		type: 'data',
	              		depth,
	              		row: dataRows.length + formattedColumnHeaders.length,
	              	})
	              }
              } else {
              	/**
              	 * Data
              	 */
              	for (let i = 0; i < dataPos.length; i++ ) {

              		dataRows.push({
	              		value: [...visibleColumns.map((col) => dataPos[i][col]), ...aggs.map((field) => dataPos[i][field])],
	              		type: 'data',
	              		depth,
	              		row: dataRows.length + formattedColumnHeaders.length,
	              	})
              	}
              	
              }

            }
          } else {
            for (const innerKey in dataPos) {
              recurseThroughMap(dataPos[innerKey], map[innerKey] || 1);
            }
          }
        })(recursedData, mapToHeader || 1);

      } else {
        const rowHeaderValue = rowHeaders.shift();
        const value = rowHeaderValue ?
            rowHeaderValue.value :
            [key, ...fillArray(headerLength - 1)];

        dataRows.push({
          value,
          depth,
          type: 'rowHeader',
          row: dataRows.length + formattedColumnHeaders.length,
        });

        rowRecurse(rowGroups[key], depth + 1, rowHeaders);
      }
    }
  }


	let dataGroups = []
	if (rows.length > 0) {
    for (let i = 0; i < rows.length; i++) {
      // possible memoization opportunity
      rowRecurse(groupByCategories(data, rows.slice(0, i + 1)), 0, dataGroups);
      dataGroups = Object.assign([], dataRows);
      if (i + 1 < rows.length) {
        dataRows = [];
        prevKey = null;
      }
    }
  }

  const colHeadersValueCopies = formattedColumnHeaders.map(({ value }) => {
    return Object.assign([], value);
  });
  const colHeadersCopy = formattedColumnHeaders.map((colHeaderObj, i) => {
    const copy = Object.assign({}, colHeaderObj);

    copy.value = colHeadersValueCopies[i];
    return copy;
  });

  return {
  	dataRows: colHeadersCopy.concat(dataRows)
  }
}