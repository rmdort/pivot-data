/**
 * @file Utility functions for collapsing and expanding functionality
*/

/**
 * Collapse a row given a row number and the data
 * @param {number} rowNum
 * @param {Array<Object>} data
 * @returns {Object}
*/
export function collapse(rowNum, data) {
  /** get the current row, row type, and row depth */
  const selectedRow = data[rowNum];
  const {type, depth} = selectedRow;

  /** if row is not a header, it is not collapsable*/
  if (type !== 'rowHeader' && type !== 'colHeader') {
    return data
  }

  let count = rowNum + 1;
  let currDepth = data[count].depth;

  /** any row above the row to collapse (including the row itself) can not collapse into the row */
  var uncollapsed = data.slice(0, count)

  const collapsed = []

  /**
   * while count is less than the number of rows
   * or while the current row is deeper than the row we are collapsing into
  */
  while (count < data.length && currDepth > depth) {
    /** roll data in collapsed state and advance pointer */
    collapsed.push(data[count]);
    count += 1;
    /** update the current depth */
    if (count < data.length) currDepth = data[count].depth;
  }

  /** remaining rows are not collapsed */
  uncollapsed = uncollapsed.concat(data.slice(count));

  return {
    uncollapsed,
    collapsed,
  };
}

/**
 * Expand a row given a row number, the current data, and the current collapsed rows
 * @param {number} rowNum
 * @param {Array<Object>} currData
 * @param {Object} collapsedRows
 * @returns {Object}
*/
export function expand(rowNum, currData, collapsedRows) {
  /** if there are no collapsed rows, then just return data */
  if (!collapsedRows) return currData;

  /** splicing the collapsed rows back onto the data table */
  currData.splice(rowNum + 1, 0, ...collapsedRows)

  return currData;
}